\version "2.18.2"

#(set-global-staff-size 16)


% the row should be written as a list of numbers (0 = c, 1 = c-sharp, etc.)
#(define row '(10 9 7 2 1 8 6 11 0 4 5 3))

\include "12tone-calc.scm"
\include "12tone-engravers.ily"

\layout {
  \context {
    \Score
    \override TimeSignature.stencil = ##f
    \override Stem.stencil = ##f
    \override BarLine.layer = 1
  }
  \accidentalStyle dodecaphonic
}

\paper {
  left-margin = 3\cm
  right-margin = 2\cm
  indent = 1.5\cm
  line-width = 16\cm
  tagline = ""
  ragged-last = ##f
}

\score {
  \new StaffGroup <<
    \time 1/4
    \set Timing.defaultBarType = "!"
    \set StaffGroup.systemStartDelimiter = #'SystemStartBar
    
    %Write indications of Inversions
    \new Lyrics {
      \markupI
    }
    
    %Write all Rows plus indications of transpositions
    \writeSquare
    
    %Write indications of Retrogrades of Inversions
    \new Lyrics {
     \markupRI 
    }
  >>
}