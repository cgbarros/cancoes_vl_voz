\version "2.19.35"

\header {
  title = "In memoriam"
  composer = "Caio Giovaneti de Barros"
  poet = "Frederico García Lorca"
  tagline = ""
}

#(set-global-staff-size 19)

\include "alternative-dynamics.ly"
\include "slash.ily"

breatheFermata = {
  \once \override BreathingSign.text = \markup { 
    \column {      
      \line { \smaller \musicglyph #"scripts.ufermata" } \vspace #-0.6
      \line { \musicglyph #"scripts.rcomma" } 
    }
  }
  \once \override BreathingSign.Y-offset = #3.2
  \breathe
}

sultSpan = \tweak bound-details.left.text #"sul t." \startTextSpan
sulpSpan = \tweak bound-details.left.text "sul p." \startTextSpan

talloneSpan = \tweak bound-details.left.text "tallone" \startTextSpan
puntaSpan = \tweak bound-details.left.text "punta" \startTextSpan

parMarcato = \markup { \normal-text \concat { \vcenter { ( \musicglyph #"scripts.umarcato" ) } } }
parSforzato = \markup { \normal-text \concat { \vcenter {  ( \musicglyph #"scripts.sforzato" ) } } }
parTenuto = \markup { \normal-text \concat { \vcenter { ( \musicglyph #"scripts.tenuto" ) } } }
papmm = \markup { poco a poco meno marcato }
senzaSord = \markup {
  \override #'(baseline-skip . 0)
  \column { 
    \line { senza }
    \line { sord. }  
  }
}


global = {
  \tempo Lento 4 = 58
  \time 5/4
  \override StaffGroup.SpanBar.stencil = ##f
  s4*5
  \time 3/4
  s2.
  \time 4/4
  s1
  \time 3/4
  s2.
  s2.
  s2.
  \time 7/8
  s8*7
  \time 5/4
  s4*5
  \time 2/4
  s2
  s2
  s2
  \time 3/4
  s2.
  \time 4/4
  s1
  \time 3/4
  s2.
  \time 5/4
  s4*5
  s4*5
  \time 3/4
  s2.
  s2.
  \time 6/4
  s1.
  \time 2/4
  s2
  \time 3/8
  s4.
  \time 3/4
  s2.
  \time 5/4
  s4*5
  \time 5/8
  s8*5
  \time 5/4
  s4*5
  \time 6/4
  s1.
  \time 2/4
  s2*4
  \time 5/8
  s8*5
  \tempo "Molto Adagio" 4 = 40
  \time 3/4
  s2.
  \tempo "Tempo I" 4 = 58
  s2.
  s2.
  s2. 
  \revert StaffGroup.SpanBar.stencil
  \bar "|."
}

voz = \relative c'' {
  bes4\p \< \tuplet 3/2 { a4 g8-- ~ } g d\mp r4 r16 bes'8.\( \p ~ 
  bes8 a \tuplet 3/2 { bes a g\> ~ } g16 d8.\) \! 
  r16\fermata d8.\p \<  \tuplet 3/2 { cis4 gis'8\mf ~ } \tuplet 3/2 { gis gis4\mp } fis8 gis16 gis\>
  fis8 b,!\! r4 \tuplet 3/2 { r8 b\( \mp \< c }
  e!8\f \> e16 f ees4.\p ees8\) \breatheFermata
  ees8.\mf e!16 ~ e4. r8 
  e16[-> \> fis b8]\! ~ \tuplet 3/2 { b8 c\> f,! } c'8[ f,]\! r8
  \tuplet 3/2 { e4\< fis b } fis\! \tuplet 3/2 { r8 b\mf \< c } f,!4
  r8 g\f d'8. g,32 g ~ 
  g16[ r16 d'8]\( ~ d cis
  a4 aes8 bes8-> ~ 
  bes4 bes4\) r4 
  r2 r8\fermata d8\pp r4
  cis-- \( \< b-- fis'8.-- \) r16
  r16 f!8.\mf \( ~ \tuplet 3/2 { f8 f!4 } \tuplet 3/2 { c!4 bes ees, ~ } ees8 ees \) \breathe
  e!4 \p \< gis8. e16 \mf \> ~ e8 gis16 \p \< a g!4.\mf g8
  r2 ees'16.\f ees32 ~ ees8 
  f4-> \tuplet 5/4 { e!8 e c e4 } 
  r8 c8\( \< b4 ~ b16 fis8. \tuplet 3/2 { gis4\f \> cis d } g,!4\mf
  a4-> \ff \> ~ a16 bes8.\p \) ~ bes4  r8\longfermata
  r4 \tuplet 3/2 { ais\mf b! cis }
  fis,8\< g ~ g g c d\f ~ d8. c16 \tuplet 3/2 { d8\< c4 }
  aes\ff aes r8\fermata
  \tuplet 3/2 { d8\p \( a gis ~ } gis8\) gis \tuplet 3/2 { gis4 e\< ees } f\mp \breathe
  \tuplet 3/2 { bes8\mf \< a g ~ } g16\! d8. bes'16\p a bes\< a \tuplet 3/2 { g8[ d des]\f \> } aes'8 aes\p ~ aes4\fermata
  r2
  \tuplet 3/2 { f4\< ees8 ~ } ees16 e!8\f \> gis16
  a32 d16. c8 g8.\p \< fis16 ~ fis8 cis'8 \f \>
  \tuplet 3/2 { b4 b'8\ppp ~ } b16[ ais8.]
  r4 r8
  gis,4\p fis8 fis16 r16 b,8 r8\fermata
  \tuplet 3/2 { r8 b-- \< c-- } e-- e16-- f!-- ees4-> \f  
  ees2 r4
  r2 r4
}

violino = \relative c' {
  \override TextSpanner.bound-details.right.text = \markup { \draw-line #'(0 . -1) }
  \override TextSpanner.bound-details.right-broken.text = \markup { \null }
  r4 \tuplet 3/2 { r4 bes8^"sord." \sultSpan \p \( ~ } bes4. a8 bes g \bar "!"
  bes4. g8 bes8 a\> \bar "!" 
  bes8\fermata \) g8\stopTextSpan \pp ~ g32\mp \> ( d' gis'' cis) cis\< ( gis d,, g,)\! r2
  \tuplet 3/2 { fis'4^"senza sord." \p \ottava #1 b''!\open \f \ottava #0 r4  } c,4\pp 
  \grace { e,,32( -> \ffp f } ees4) ~ ees8 ees-- ( \< ~ \tuplet 3/2 { ees8 ees-- ees--) \mf }
  r2 r4 
  e'!16[( \downbow \mp fis b-. b]-. c8[) \tuplet 3/2 { f,,16( \upbow -> ees e!]) ~ } e4 ~ e8 ~
  e16(\p fis b,) r16 r4 \tuplet 6/4 { r16 f'''!16[( \ppp ees e! fis b]) } r4 \tuplet 3/2 { r8 d,,-.\f d-. } \bar "!"
  g4-- \tuplet 3/2 { bes8\upbow ( a c } \bar "!"
  \tuplet 3/2 { bes8) g-. a -- ~ } \tuplet 3/2 { a8 bes4-- } 
  a4-- \tuplet 3/2 { c8\downbow -^ bes\downbow -^ a\downbow -^  }
  <<
    {
      \oneVoice
      \tuplet 3/2 { <g,, d' ees' g>8-> \arpeggio \downbow f'16( \upbow ees d8) ~ } d2 \> 
      \override Beam.grow-direction = #RIGHT
      \override TupletNumber.text = \markup { \note #"2" #UP }
      \override TupletNumber.Y-offset = #2.5
      \override TupletBracket.bracket-visibility = ##t
      \tuplet 7/8 { d16[ s d s d s d] ~ } d4\p \fermata
      \override TupletNumber.text = \markup { \note #"4" #UP }
      \tuplet 5/4 { d16[ s d s d] }
    }
    \\
    {
      s2.
      \override Beam.grow-direction = #RIGHT
      \override TupletNumber.stencil = ##f
      \override TupletBracket.stencil = # #f
      \tuplet 7/8 { s16 d[ s d s d] s }
      s4
      \tuplet 5/4 { s16 d[ s d] s }
    }
  >>
  \tuplet 3/2 { cis''4\> b' fis,,\pp \< ~ } fis4
  d''8 fis, \mp \> ~ fis f! c'4\p r8 \ottava #1 bes'8\p ~ bes ees\upbow \ottava #0 \breathe 
  e,,16( \> fis) b\! r16 \tuplet 3/2 { r8 gis,\mf a } g!32\upbow ( f! e8.) ~ e2
  ees8-. -> \f \talloneSpan r \tuplet 3/2 { ees8[-. -> r ees]-. -> } ees16[-. -> r r ees]-. ->
  f4:32\ffp \< \stopTextSpan 
  \once \override TextScript.outside-staff-priority = #-999
  e8:32^"punta" \sulpSpan c'8:32 b8:32 fis'8:32 
  gis'4:32\mf \stopTextSpan r4 cis,,,8^"pizz." \f \autoBeamOff r8 r8 cis cis r r16 cis8 r16 \autoBeamOn
  r8 e'16( -> ^"arco" \mp fis g b!-> a8 \bar "!"
  fis16[ -> e fis a] -> g8 ) \longfermata
  \grace { \slash e,8( -> \sfz \> d e fis } e8[ ais,] ~ ais)\p r8  b''8\fp \> ~ b32 cis fis-. fis-.\ppp
  r4 r8 g,,,8\pp ~ g4 <g d'> ~ 
  \override Beam.grow-direction = #LEFT
  \override TupletBracket.bracket-visibility = ##t
  \override TupletNumber.text = \markup { \note #"4" #UP }
  \tuplet 4/4 { q16 <g d' c'>\arpeggio a''\open aes' }
  \revert Beam.grow-direction
  \revert TupletBracket.bracket-visibility
  \revert TupletNumber.text
  r2 <d,,, a'>8\upbow \fermata ~
  q8. r16 gis'32\pp a e ees r8 r2 r4 
  bes'8[^"sord" ( \pp \< a] bes[ d,]-> \p ~ d[ des] aes'4.\pp \> ges8) \! r4\fermata
  \tuplet 3/2 { b,!8^\senzaSord \p c e, ~ } e16 f8. ^\parMarcato ^\papmm \ffdim ~ 
  f8 ees16. f32 \tuplet 3/2 { e!8 gis4 }
  \once \override TextScript.X-offset = #-1
  \tuplet 5/4 { a16^\parSforzato gis' a, d c' } 
  <<
    {
      \oneVoice
      \once \override TextScript.X-offset = #-0.7
      \tuplet 3/2 { g'8^\parTenuto fis,,8 g ~ }
      g32 fis'\p \> cis'8 fis,16 
      \tupletDown
      \tuplet 3/2 { cis,4 <b' e\harmonic>8\pp ~ }
      <b e\harmonic>4
    }
    \\
    {
      s2
      \override Stem.stencil = ##f
      \override Flag.stencil = ##f
      \override TupletNumber.stencil = ##f
      
      \tuplet 3/2 { s4 \parenthesize b'8^\markup { \center-align 15ma } }
    }
  >>
  r4 r8 
  <b,, gis'>4\pp <b fis'>8 <c fis>16 r16 r8 <c e>8\fermata 
  r2 ees4\mp 
  d16( -> ^"pizz." \p d a d-> e8 fis16-> fis ~ fis4)
  d16( -> d a d-> e8 fis16-> fis ~ fis4)
}

verse = \lyricmode {
  Dul -- ce cho -- po, Dul -- 
  _ _ _ _ _ _ _ _ (ul) -- _ ce cho -- 
  _ po, Te has
  pues -- to De o -- ro.
  A -- yer
  es -- _ ta -- _ bas ver -- de,
  Un __ _ ver -- de lo -- _ co
  De pá -- ja -- ros 
  Glo -- _ _ ri -- o -- sos.
  Hoy
  es -- tás __ _
  a -- ba -- ti -- _ _ do
  Ba -- jo~el cie -- lo de~a -- gos -- to
  Co -- mo 
  yo fren -- te~al cie -- lo 
  De mi~es -- pí -- _ ri  _ tu
  ro -- jo.
  La fra -- gan --
  _ _ cia cau -- ti -- va De tu
  tron -- co
  Ven -- _ drá a mi co -- ra -- zón
  Pi -- a -- do -- _ _ _ _ _ _ _ _ _ so.
  ¡Ru -- do~a -- bue -- _ 
  _ _ lo del pra -- _ _ _
  do!
  No -- so -- tros, Nos
  he -- mos pues -- to De o -- ro.
}

\score {
  \new StaffGroup <<
    \accidentalStyle StaffGroup.forget
    \set Timing.defaultBarType = ""
    \new Dynamics { \global }
    \new Staff \with {
      \consists "Bar_engraver"
      instrumentName = "Voz"
    } { 
      \override Staff.BarLine.stencil = ##f
      \voz 
      \revert Staff.BarLine.stencil
    }
    \addlyrics { \verse }
    \new Staff \with {
      instrumentName = "Violino"
      \consists "Bar_engraver"
    } { \violino }
  >>
  \layout { 
    \context { 
      \Score
      \remove "Bar_number_engraver"
      \override TimeSignature.stencil = ##f
      \override TextScript.font-shape = #'italic
    }
  }
}

\markup {
 \fill-line { \null "São Paulo, 17 de Fevereiro de 2016." } 
}