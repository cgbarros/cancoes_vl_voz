# Songs for violin and voice

These songs are written using poems by Frederico García Lorca.

Engraved using [Lilypond](http://lilypond.org)

## Voice range

Voice range goes from B3 (below middle C) to B5.

## Licence

Code is licenced under GPL-3.0-only - see the [LICENSE.md](LICENSE.md) file for details.

Music is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License.](http://creativecommons.org/licenses/by-sa/4.0/)