\version "2.19.46"

\include "tempoCa.ly"
\include  "alternative-dynamics.ly"

#(set-global-staff-size 19)

tempoI = \tempoCa "Lugubre" 4 45

global = {
  \tempo \tempoI
  \time 5/4
  s4*5 | %1
  \time 3/4
  s2. | %2
  \time 4/4
  s1 | %3
  \time 3/4
  s2. | %4
  \time 4/4
  s1*3 | %5-7
  \time 2/4
  s2 | %8
  \time 2/4
  s2 | %9
  \time 4/4
  s1 | %10
  \time 6/4
  s1. | %11
  \time 4/4
  s1 | %12
  \time 9/8
  s8*9*2 | %13-14
  \time 2/4
  s2 | %15
  \time 4/4
  s1 | %16
  \time 5/4
  s4*5 | %17
  \time 4/4
  s1 | %18
  \time 5/4
  s4*5 | %19
  \time 4/4
  s1 \bar "|." %20
}

voz = \relative {
  r1 r2
  r4 fis'8\psempre 8 ~ | %2
  fis16 16 16 16 8. 16 r4 \tuplet 3/2 { b4 fis8 } | %3
  fis16 16 8 ~ 8 8 r4 | %4
  \tuplet 3/2 { gis8\< 8 8 } \tuplet 3/2 { 8 8 cis ~ } cis8\f cis  r4 | %5
  r2 fis,8\p 8 16 8 16 | %6
  r4 \tuplet 3/2 { b8\< 8 fis ~ } fis e'\fpsub ~ 16 a,8 bes16 | %7
  d8( \< ees des16) 16\mf r8 | %8
  \tuplet 3/2 { ees4\mf f ees } | %9
  f,8 d'4 ees des16 c c4 | %10
  r4 c8\p \< \tuplet 3/2 { des16 c8 } ees8. b16\! ~ b8 f\mf \> c'4\p r4\fermata | %11
  c8.\f 32 32 r4 \tuplet 5/4 { d8\p \< g, gis cis dis ~ } | %12
  dis8[\mf 16 16] e8-> g,4 g8 r4 r8 | %13
  gis8[\> 8 8 cis] b8 b16[ fis8.] fis4\p | %14
  fis8\psempre 8 8 4 8 r\breve | %16
  r4\fermata gis8\psempre 8 ~ 16 16 16 16 8. 16 | %18
  r4 cis8[\f \> gis dis gis] \tuplet 3/2 { gis2\pp gis4 } | %19
  r1 | %20
}

texto = \lyricmode {
  4*5 | %1
  2 Los8 cien8 | %2
  16 e -- na -- mo -- ra8. dos16 4 \tuplet 3/2 { duer4 -- men8 } | %3
  pa16 -- ra siem4 -- pre8 4 | %4
  \tuplet 3/2 { ba8 -- jo la } \tuplet 3/2 { tier8 -- ra se -- } _8 ca8 4 | %5
  2 An8 -- da -- lu16 -- cí8 -- a16 | %6
  4 \tuplet 3/2 { tie8 -- ne  lar -- } _4 _16 gos8 ca16 -- | %7
  mi4 -- _16 nos16 8 | %8
  ro2 -- | %9
  _2. jos4 | %10
  4 \markup { \italic (chiusa) } 1 4 | %11
  Cór8. -- do32 -- ba32 4 o2 -- | %12
  _8 li16 -- vos16 ver4. -- des8 4 8 | %13
  don8 -- de po ner cien cru4 -- ces | %14
  que8 los re -- cuer4 -- den8
  1 1 | %17
  4 Los8 cien8. e16 -- na -- mo -- ra8. -- dos16 | %18
  4 duer8 -- men pa -- ra \tuplet 3/2 { siem2 pre4 }
}

pizzME = \markup {
  \override #'(baseline-skip . 2)
  \column {
    \line { \italic "pizz. mano sinistra" }
    \line { \italic "e arco" }
    \line { \italic "colla voce" }
  }
}

fn = "* a cantora posiciona a surdina enquanto o violino soa"

vl = \relative {
  ees'''2\ppp \< f,,4( e!8. \> c'16 ~ c4)\! | %1
  \tuplet 3/2 { c4(\< b fis\> ~ } fis4)\! | %2
  \tuplet 3/2 { gis,4\< cis8\f \> ~ } cis8. d''16 ~ d2\pp | %3
  cis8(\p d ~ d2) | %4
  gis,,,8.( cis16) cis''8 d \tuplet 3/2 { g,4\< a,, bes } | %5
  \override Beam.grow-direction = #LEFT
  a16( ^\markup {\italic "(rit.)"} bes des ees'' ~ ees4)\fermata \f d2\open \psub | %6
  bes,,16( \f \> a e'' fis \ottava #1 b'4.)\open \p \ottava #0 c,8\mf f,8.(-> g,,16) | %7
  \revert Beam.grow-direction
  \tuplet 3/2 { gis8( \> a \ottava #1 b''')\open \pp ~  } b4 ~ | %8
  b8 \ottava #0 bes,,,\< ges'4\! | %9
  f c'16 d, g,8 ~ g \acciaccatura aes des'8 ~ des8. e16 | %10
  f,1.\fermata \breathe | %11
  << 
    {  
      c'2^\pizzME \laissezVibrer d4 \laissezVibrer
    } 
    \\ 
    \relative {  
      c''2\mf \tuplet 5/4 { d8(\p \< g, gis cis \oneVoice dis ~ } | %12
      dis8[)\mf dis16 dis]
    } 
  >>
  e4(-> \> d,4 ees''4.)\pp | %13
  \tuplet 3/2 { gis,,4-> fis \stemDown fis } r8 \tuplet 3/2 { \stemUp fis4 \stemDown fis \stemUp fis } | %14
  \tuplet 3/2 { \stemDown fis4 \stemNeutral fis8 } g8\fp gis\< ~  | %15
  gis4\> ~ gis8\! gis \once \override Beam.grow-direction = #LEFT cis16( b' ges' f ~ f4)\fermata | %16
  c,,16\f \> bes8. a8. ees'''16\! ~ ees8 f,, \tuplet 3/2 { e'4 c'\> gis, } | %17
  \ottava #1
  \override Score.FootnoteItem.annotation-line = ##f
  \override Score.FootnoteItem.extra-offset = #'(-1 . 0)
  b''1\open \fermata -\footnote "*" #'(0.1 . 0.1) \fn ^"sord." \p ~ | %18
  b4\open \ottava #0 fis,,8[\f \> gis cis d]\! \tuplet 3/2 { g,,2\pp \> a'4 } | %19
  bes'1\ppppp \longfermata | %20
}

\header {
  title = "De Profundis"
  poet = "Frederico García Lorca"
  composer = "Caio Giovaneti de Barros"
  tagline = ""
}

\score {
  \new PianoStaff <<
    \set Timing.defaultBarType = ""
    \accidentalStyle Score.forget
    \new Dynamics \global
    \new Staff \voz
    \new Lyrics \texto
    \new Staff \vl
  >>
}

\markup {
  \fill-line {
   \null "São Paulo, 25 de Junho de 2016" 
  }
}

\layout {
  \context {
    \Staff
    \override Hairpin.minimum-length = #7
    \override TimeSignature.stencil = ##f
  }
}

\paper {
  ragged-last-bottom = ##f
}