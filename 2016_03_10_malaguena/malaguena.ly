\version "2.19.35"

\include "arrow-accidentals.ily"
\include "slash.ily"
\include "alternative-dynamics.ly"

\header { 
  title = "Malagueña"
  composer = "Caio Giovaneti de Barros"
  poet = "Frederico García Lorca"
  tagline = "" 
}

#(set-global-staff-size 18.1)

sulgSpan = \tweak bound-details.left.text #"sul G" \startTextSpan
sulpSpan = \tweak bound-details.left.text #"sul p." \startTextSpan

glissandoSkipOn = \override NoteColumn.glissando-skip = ##t
glissandoSkipOff = \revert NoteColumn.glissando-skip

glissSord = \markup {
  \override #'(baseline-skip . 0.1)
  \column {
    \line { "con sord." }
    \line { \italic "gliss sul D" }
  }
}

battuto = \markup {
  \override #'(baseline-skip . 0.1)
  \column {
    \line { "senza sord." }
    \line { \italic "col legno battuto" }
  }
}

glissSulA = \markup { \italic "gliss sul A" }

percNoteHead = {
  \once \override NoteHead.style = #'cross 
  \once \override NoteHead.no-ledgers = ##t
  \once \override NoteHead.font-size = #2
  \once \override Stem.length = #15
}

global = {
  \tempo Agitato 4 = 120
  \time 3/8
  s4.
  \time 3/4
  s2.
  \time 3/8
  s4.
  \time 3/4
  s2.
  \time 5/8
  s8*5
  s8*5
  \time 3/4
  s2.
  \time 3/8
  s4.
  \time 4/4
  s1 \bar "||"
  \override Score.TimeSignature.stencil = ##f
  \override Score.BarNumber.stencil = ##f
  \time 9/8
  \tempo "più lento e rubato"
  s1 s8 \bar "||"
  \revert Score.TimeSignature.stencil
  \revert Score.BarNumber.stencil
  \time 3/8
  \tempo "tempo primo"
  s4.
  s4.
  s4.
  \time 4/4
  s8
  \tempo "più grave"
  s2..
  \time 3/8
  s8
  \tempo "a tempo"
  s4
  \time 2/4
  s2
  \time 3/8
  s4.
  \time 5/8
  s8*5 \bar "||"
  \override Score.TimeSignature.stencil = ##f
  \override Score.BarNumber.stencil = ##f
  \time 5/4
  \tempo "meno mosso"
  s4*5 \bar""
  \time 4/4
  \tempo "quasi staccato"
  s1 \bar""
  \time 5/8
  s8*5 \bar""
  \time 4/4
  s1 \bar "||"
  \revert Score.TimeSignature.stencil
  \revert Score.BarNumber.stencil
  \time 3/4
  \tempo "tempo primo"
  \set Score.currentBarNumber = #20
  s2.
  \time 3/8
  s4.
  \time 2/4
  s4.
  \tempo "sordo, poco meno mosso"
  s8
  \time 3/8
  s4.
  s4.
  \time 2/4
  s2
  s2 
  \time 5/8
  s8*5
  \time 4/4
  s1
  \time 3/8
  s4.
  \time 3/4
  s2.
  \time 2/4
  \grace { s4 }
  s2
  s2
  \time 3/4
  s2.
  \time 5/8
  \grace { s8 }
  s8*5
  \time 5/4
  \tempo "tempo primo"
  s4*5
  \time 5/8
  s8*5
  \time 3/4
  s2.
  \time 4/4
  \tempo "grave, più lento"
  s1
  \time 5/8
  \tempo "molto energico, veloce"
  s8*5
  s8*5
  \time 4/4
  \tempo "grave, più lento"
  s1
  \time 3/4
  s2.
  \time 3/8
  \tempo "tempo pimo"
  s4.
  \time 3/4
  s2.
  \time 5/8
  s8*5
  \time 7/8
  s8*7 \bar "||"
  \override Score.TimeSignature.stencil = ##f
  \override Score.BarNumber.stencil = ##f
  \time 5/8
  \tempo \markup { \override #'(baseline-skip . 0)
   \column {
     \line { "più lento y rubato," }
     \line { "come una improvvisazione"   }
   }
  }
  s8*5 \bar ""
  \time 5/4
  \set Score.currentBarNumber = #47
  s4*5 \bar "||"
  \time 5/8
  \set Score.currentBarNumber = #48
  \tempo "tempo primo"
  s8*5 \bar "|."
}

voz = \relative c' {
  R4. |
  R2. |
  R4. |
  R2. |
  r8 cis\f \> fis4 ~ fis16 fis\! |
  r8 r4 a8-. \f bes-. |
  gis4 ~ gis16[ gis r16 gis] ~ \tuplet 3/2 { gis8 gis\< gis\( } |
  e'4\! eeh32-> e! eeh16 ~ |
  \tuplet 3/2 { eeh4 ees2 \p ~ } ees2 ~ |
  \tuplet 5/4 { ees16\fermata \< des ees eesih desih\f \> ~ } desih4\! des4\p des8.\) r16  r8 |
  ees8-_ \mf \< ees-_ ees-_ |
  des8 \! \( aes4\) \> |
  d!8\mf \> \( aes4 \) \! |
  r8\fermata  f8\( \< f8. f16 g8 b\f ~ b32 c8 b32 c16 |
  bes8-.\) bes[-_ bes-_]
  f4 f8\< f |
  g8-> \! f4 |
  g8-> f4 f |
  \override TupletNumber.stencil = ##f
  ges2. \> ~ \grace { \slash \tuplet 5/4 { ges8 fih\sfz ges gesih f! ~ } } f2\fermata \breathe
  \revert TupletNumber.stencil
  f!16\mp ges8. ~ ges8 bes8 \tuplet 3/2 { c4 bes8 \< ~ } bes4\shortfermata |
  aes\mp \grace { \slash aeh8( -> a! } aeh4.) |
  aes2 \acciaccatura aesih8 \tuplet 3/2 { aes4-> ges2\shortfermata \espressivo }
  f2.\fsub |
  R4. |
  r4 r8 f8-. \( \mf  |
  g-. g-. g-. \) |
  cis8-> \( g4 |
  f\) f8 \( g |
  cis-> g4 \) g8\( \< |
  cis\f dis4 ~ dis16 \) f,8. \mf \( |
  g4 \) \tuplet 3/2 { dis'8\f \( cis4 ~ } cis8 g \> g4 \) |
  g4-.\mf ^\( g8-. |
  \override Script.outside-staff-priority = #1000
  a4_- a-- ~ a16 \) b8.\( \> |
  \revert Script.outside-staff-priority
  \grace { \slash c8( des } c4) ~ \tuplet 3/2 { c8 b4\) \! } |
  r8 c-.\(  \< c-. c-. |
  \tuplet 3/2 { c2 \> \slashedGrace cih8 bes4\! ~ } bes4 |
  \slashedGrace ceh8-> b!4 \slashedGrace ceh8->  b8.[ bes16] ~ bes8\) |
  R4*5 |
  r8 r4 r8 gis'8\( \ff |
  gis4 ~ gis16 gis16\) r8 r4 |
  cis,4.\( \mf \< \tuplet 3/2 { cisih16 cis ciseh ~ } ciseh4. \> c!8-. \! \) |
  g!8 \f \< -_ g'!-_ \> g,-_ \! r4 |
  r4 g8[-_ \< g'-_ \> g,]-_ \! |
  c!4.\( \mf \< \tuplet 3/2 { ciseh16 cis cisih ~ } cisih4\> cis8-.\) gis'\ffsub \( |
  gis4 ~ gis16 gis16 \) r8 r4 |
  d,8-_ \f \< d-_ d-_ |
  ees2.\ff ~ |
  ees8  ~ ees2 ~ |
  ees8 ~ ees2. ~ |
  ees8 f g a4 |
  g4 r4\shortfermata \grace { \slash geh8-> g! ~ } \tuplet 3/2 { g4 f8 ~ } f4 \grace { \slash ees8\< eesih } ees4 ~ |
  ees4.\! d8 r8 |
}

texto = \lyricmode {
  %\override LyricHyphen.minimum-distance = #1.0
  La  muer -- te
  en -- tra~y 
  sa -- le de la ta --
  ber -- _ _ _ _ _ _ _ _ _
  na.
  Pa -- san ca --
  ba -- llos
  ne -- gros
  y gen -- te si -- nies -- _ _ _
  tra por los
  hon -- dos ca --
  mi -- nos
  de la gui -- 
  ta -- _ _ _ _ _ _ _ _ _ _ _ _ _ _ rra.
  y hay un o -- 
  lor a
  sal y a
  san -- gre de
  hem -- _ _ _ _ _ _ bra,
  en los
  nar -- dos fe --
  bri  -- les
  de la na --
  ri -- _ na. __ _ _ _ _ _
  La muer -- te
  en -- _ _ _ tra
  y sa -- le,
  y sa -- le~y
  en -- _ _ _ tra la
  muer -- te
  de la ta -- 
  ber -- _ _ _ _ _ _ _ _ _ _ na
}

violino = \relative c' {
  \override TextSpanner.bound-details.right.text = \markup { \draw-line #'(0 . -1) }
  \override TextSpanner.bound-details.right-broken.text = \markup { \null }
  r8 ees16\ff( d c8) -> |
  <g d' ees' c'>4-> \downbow  q4-> \downbow q-> \downbow |
  r8 q4-> \downbow |
  \once \override Hairpin.minimum-length = #10
  <g fis'>2\ppp \upbow ~ \< q4 \fff |
  <g fis' cis'>-> r4 fis'''16[ ( \pp cis' |
  b e,]) r4 <b, e f'>4-> \fff |
  <e f'>2\downbow \> ~ q4\ppp |
  <f, a bes'>4-> \ff r8 |
  aes,4-> \downbow aes-> \downbow aes-> \downbow aes8( -> bes16 a!) \ppsub ~ |
  a1 \fermata
  \once \override Glissando.minimum-length = #5
  \once \override Glissando.springs-and-rods = #ly:spanner::set-spacing-rods
  e''16\p \glissando ees' 
  r8 <g,,, ees'>4-- \f |
  q4-- <g des''>8-. \upbow |
  \autoBeamOff
  <g aes'>4( q8[ ~ |
  q8] \fermata q8 ~ q4 ~ q8) \autoBeamOn d'16( \< \sulgSpan  c) r8 f16( ges) \fff \stopTextSpan 
  \ottava #1
  bes''4.\ppp \< ( ~ |
  bes8 bes4 bes8 ~ |
  bes8) b!8-. \upbow a8-- \mp ~ |
  a8 a4-- \ottava #0 r4 |
  ges,,2.\> \grace { \slash gesih8[\sfz aes gesih ges] ~ } ges4 f!4\fermata
  s1^"tacet" |
  s8*5 |
  s1 |
  << 
    {
      \oneVoice
      \override Glissando.minimum-length = #5
      \override Glissando.springs-and-rods = #ly:spanner::set-spacing-rods
      \tuplet 3/2 { f8\f ( \glissando ges \glissando f) ~ } <f b\harmonic>2\< 
      \revert Glissando.minimum-length
      \revert Glissando.springs-and-rods
      \tuplet 3/2 { f16(\ff \downbow g \> fis) } a,4\upbow |
      a32-.( \mf \downbow ^"jeté" a-. a-. a-.) r8 a32-.( a-. a-. a-.) r8
    }
    \\
    {
      \oneVoice
      \override Stem.stencil = ##f
      \override NoteColumn.ignore-collision = ##t
      s4 \small \parenthesize f''' s4
      s4.
    }
  >>
  R4. |
  \stemDown
  \ottava #1
  d,32[ \pp \open ^\glissSord  a'\open d\open fis\open \glissando \glissandoSkipOn \hide Rest r32] \undo \hide Rest s16. \glissandoSkipOff s8
  a'32\open \glissando s16. s8 s8 d,,,32\open \glissando s16. |
  s8 s8 a'''32\open \glissando s16. s8 |
  s8 d,,,32\open \glissando s16. s8 s8 a'''32\open \glissando  s16 \parenthesize d,,,32\open |
  \ottava #0
  \stemNeutral
  ais4\sulpSpan -> \downbow \f r4 r16 dis( \sfz \upbow e) r16 r4 |
  a,,4-> \downbow \stopTextSpan \stemDown \ottava #1 
  \once \override TextScript.outside-staff-priority = #0
  a'!32[ ^\glissSulA \pp \open e'\open a\open cis32*1/2\open \glissando \glissandoSkipOn \hide Rest r64] \undo \hide Rest |
  \glissandoSkipOff
  s8 s8 e'32\open \glissando s16. s8 s8 a,,,32\open \glissando s16. |
  s8 s8 e'''32\open \glissando s16. s8 |
  s8 a,,,32\open \glissando s16. s8 s16. \parenthesize e'''32\open |
  \ottava #0
  \stemNeutral
  r2 r8 \ottava #1 b32( ^\battuto -. b-. b-. b-. ) |
  r8 b32 ( -. b-. b-. b-.) c4^"arco" \tuplet 3/2 { aes16 ( bes a) } |
  \ottava #0
  <gis,, a bes'>4\ffsub \downbow q\downbow q\downbow q\downbow q\downbow |
  r8 q4\downbow q\downbow |
  r2 
  <<
    {
      \once \override Glissando.minimum-length = #6
      \once \override Glissando.springs-and-rods = #ly:spanner::set-spacing-rods
      \tuplet 3/2 { f'8\f \< \glissando aeseh \> g } |
      e1\mf
    }
    \\
    {
      e4 |
      cis4.^"colla voce" \tuplet 3/2 { cisih16( cis ciseh ~ } ciseh4. c!8^.) |
    }
  >>
  e8 \glissando \pp \< f' \! r8 b,4\open \ppp ~ |
  b2\open e8 |
  b,,8 -> \f \> \downbow cis4\mf \downbow \tuplet 3/2 { ciseh16( ^"colla voce" cis cisih ~ } cisih4 cis8-.)
  << 
    {  
      \override Stem.stencil = ##f
      \override Flag.stencil = ##f
      \override NoteColumn.ignore-collision = ##t
      \small \parenthesize gis''8
    } 
    \\ 
    {  
      \oneVoice
      <gis,, c\harmonic>8\pp ~ 
      <gis c\harmonic>4
    } 
  >>
  fis'''2 _"cresc. molto" \< ~ |
  fis4. |
  <g,,, d' ees' c'>4\ff \downbow q\downbow q\downbow |
  \percNoteHead
  a8^"*" <g d' ees' c'>4\downbow r4 |
  \percNoteHead
  a8 <g d' ees' c'>4\downbow q4\downbow r4 |
  s8*5^"tacet"
  s4*5
  q4\downbow q8\downbow \percNoteHead a4 |
}

\new StaffGroup <<
  \override Score.Hairpin.minimum-length = #5
  \override Score.Tie #'layer = #2
  \override Score.TimeSignature #'layer = #3
  \override Score.StaffSymbol #'layer = #4
  \override Score.TimeSignature #'whiteout = ##t
  \accidentalStyle StaffGroup.forget
  \new Dynamics { \global }
  \new Staff << 
    \new Voice = voz { \voz  }
    \new Lyrics \with { includeGraceNotes = ##t }
    \lyricsto voz { \texto }
  >>
  \new Staff { \violino }
>>

\layout {
 \context {
  \Staff
  \numericTimeSignature 
  \override Hairpin.to-barline = ##f
 }
}

\markup {
 * golpe com a mão no corpo do instrumento. 
}

\markup {
 \fill-line { \null "São Paulo, 10 de Março de 2016" } 
}
