\version "2.19.82"

%{
La guitarra,
hace llorar a los sueños.
El sollozo de las almas
perdidas,
se escapa por su boca
redonda.
Y como la tarántula
teje una gran estrella
para cazar suspiros,
que flotan en su negro
aljibe de madera.
 %}
 
 \include "tempoCa.ly"
\include  "alternative-dynamics.ly"

#(set-global-staff-size 19)

tempoI = \tempoCa "Inquieto" 4 85

global = {
  \tempo \tempoI
  \time 3/8
  s4. | %1
  \time 3/4
  s2. | %2
  \time 2/8
  s4*4 | %3-6
  \time 4/4
  s1
  
}

voz = \relative {

}

texto = \lyricmode {

}

vl = \relative {
 <dis' gis>4.:32\fff | %1
 <e' f>2.:32 | %2
 dis,32( gis f' e) r8 | %3
 dis,32( gis f' e) r8 | %4
 r4 | %5
 dis,32[(^"pizz." gis f' e) <e f>8:32]^"arco" \fpp
}

\header {
  title = "Las seis cuerdas"
  poet = "Frederico García Lorca"
  composer = "Caio Giovaneti de Barros"
  tagline = ""
}

\score {
  \new PianoStaff <<
    \set Timing.defaultBarType = ""
    \accidentalStyle Score.forget
    \new Dynamics \global
    \new Staff \voz
    \new Lyrics \texto
    \new Staff \vl
  >>
}

\markup {
  \fill-line {
   \null "São Paulo, 25 de Junho de 2016" 
  }
}

\layout {
  \context {
    \Staff
    \override Hairpin.minimum-length = #7
    \override TimeSignature.stencil = ##f
  }
}

\paper {
  ragged-last-bottom = ##f
}